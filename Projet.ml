(*
Projet LTPF 2022
Auteur : Florian MACHENAUD   
Co-auteur : Yann Enezoumou
co-auteur Kurklu Fikret
*)
(*-----------------------------------Fichiers Necessaires (anacomb)--------------------------------*)

exception Echec;;

type analist = char list -> char list
let epsilon : analist = fun l -> l
let terminal (c : 't) : analist = function
  | x :: l when x = c -> l
  | _ -> raise Echec
;;

(* a1 suivi de a2 *)
let (-->) (a1 : analist) (a2 : analist) : analist =
  fun l -> let l = a1 l in a2 l
(* Choix entre a1 ou a2 *)
let (-|) (a1 : analist) (a2 : analist) : analist =
  fun l -> try a1 l with Echec -> a2 l

(*------------------------------Partie ranalist*------------------------------*)

type 'res ranalist = char list -> 'res * char list
let epsilon_res (info : 'res) : 'res ranalist =
  fun l -> (info, l)
(* Terminal conditionnel avec résultat *)
let terminal_res (f : char -> 'res option) : 'res ranalist =
  fun l -> match l with
  | x :: l -> (match f x with Some y -> y, l | None -> raise Echec)
  | _ -> raise Echec
(* Choix entre a1 ou a2 informatifs *) 
let (+|) (a1 : 'res ranalist) (a2 : 'res ranalist) : 'res ranalist =
  fun l -> try a1 l with Echec -> a2 l
(* a1 sans résultat suivi de a2 donnant un résultat *)
let ( -+>) (a1 : analist) (a2 : 'res ranalist) : 'res ranalist =
  fun l -> let l = a1 l in a2 l
(* a1 rendant un résultat suivi de a2 sans résultat *)
let (+->) (a1 : 'res ranalist) (a2 : analist) : analist =
  fun l -> let (x, l) = a1 l in a2 l
(* a1 rendant un résultat suivi de a2 rendant un résultat *)
let (++>) (a1 : 'resa ranalist) (a2 : 'resa -> 'resb ranalist) : 'resb ranalist =
  fun l -> let (x, l) = a1 l in a2 x l
let rec star (a : analist) : analist = fun l -> l |>
( a --> star a ) -| epsilon
;;
let rec terminal2 (c : 't) : analist = fun l -> match l with
| x :: suiv when x = c -> suiv
| _ -> if (star (terminal ' ' -| terminal '\n' -| terminal '\r') l) = l then raise Echec else let l = star (terminal ' ' -| terminal '\n' -| terminal '\r') l in terminal2 c l
;;

(*-----------------------------------------------------------------------*)

  let list_of_string s =
    let n = String.length s in
    let rec boucle i =
      if i = n then [] else s.[i] :: boucle (i+1) in boucle 0;;
;;

(*Hierarcie de type*)
type var =
  | A
  | B
  | C
  | D


type expr = 
  | Var of var
  | BTrue
  | BFalse
  | Or of expr * expr
  | And of expr * expr
  | Not of expr



type winstr =
  | Skip
  | Assign of var * expr
  | Seq of winstr * winstr
  | If of expr * winstr * winstr
  | While of expr * winstr



(*------------------------------Partie Analist------------------------------*)
(*Grammaire :

p_var = 'a' | 'b' | 'c' | 'd'
p_bool = '0' | '1'
p_fact_affect = p_var | p_bool

--Gestion opérations booléennes--

p_N = '!(' p_E ')'
p_A = p_fact_affect '.' p_E
p_O = p_fact_affect '+' p_E
p_E = p_O | p_A | p_N | p_fact_affect

p_affect = p_var ':=' p_E

--Gestion séquence--

p_while = 'w(' p_E '){' p_seq '}'
p_if = 'i(' p_E '){' p_seq '}{' p_seq '}'
p_instr = p_affect | p_if | p_while
p_seq = p_instr  p_seq | ';' p_seq | epsilon

*)
let p_var : analist = terminal2 'a' -| terminal2 'b' -| terminal2 'c' -| terminal2 'd' ;;
let p_bool : analist = terminal2 '0' -| terminal2 '1' ;;
let p_fact_affect : analist = p_var -| p_bool;;

(*Opérations booléennes *)
let rec p_E : analist = fun l -> (p_O -| p_A -| p_N -| p_fact_affect) l
      and 
      p_O : analist = fun l -> (p_fact_affect --> terminal2 '+' --> p_E) l
      and 
      p_A : analist = fun l -> (p_fact_affect --> terminal2 '.' --> p_E) l
      and 
      p_N : analist = fun l-> (terminal2 '!' --> terminal2 '(' --> p_E --> terminal2 ')') l
;;
(*-------------------*)
let p_affect : analist = (p_var --> terminal2 ':' --> terminal2 '=' --> p_E);;
let rec p_seq : analist = fun l -> (p_instr --> p_seq -| terminal2 ';' --> p_seq -| epsilon) l 
  and
  p_instr : analist = fun l -> (p_affect -| p_if -| p_while) l
    and
      p_if : analist = fun l -> (terminal2 'i' --> terminal2 '(' --> p_E --> terminal2 ')' --> terminal2 '{' --> p_seq --> terminal2 '}' --> terminal2 '{' --> p_seq --> terminal2 '}') l
    and
      p_while : analist = fun l -> (terminal2 'w' --> terminal2 '(' --> p_E --> terminal2 ')' --> terminal2 '{'--> p_seq --> terminal2 '}') l
;;

(*tetst star*)
 (*---------*)
(*Tests Analist OK*)

assert(p_var (list_of_string "asuite") = list_of_string ("suite"));; (*test variable*)
assert(p_bool (list_of_string "1suite") = list_of_string ("suite"));; (*test booléen*)

assert(p_affect (list_of_string "a:=1suite") = list_of_string ("suite"));; (*test affectation*)
assert(p_while (list_of_string "w(c){a:=1}suite") = list_of_string ("suite"));; (*test while*)
assert(p_if (list_of_string "i(c){a:=1}{b:=0}suite") = list_of_string ("suite"));; (*test if*)

assert(p_instr (list_of_string "w(c){a:=1;}suite") = list_of_string ("suite"));; (*test instruction*)
assert(p_instr (list_of_string "i(c){a:=1}{b:=0}suite") = list_of_string ("suite"));; (*test instruction*)
assert(p_instr (list_of_string "a:=1suite") = list_of_string ("suite"));; (*test instruction*)

assert(p_seq (list_of_string "a:=1;b:=1;c:=1;w(a){i(c){c:=0;a:=b}{b:=0;c:=a}}suite") = list_of_string ("suite"));; (*test seq*)

(*----------------------------------------------------------------------------*)

(*Partie Ranalist*)
let pr_var : 'res ranalist = (terminal2 'a' -+> epsilon_res A) +| (terminal2 'b' -+> epsilon_res B) +| (terminal2 'c' -+> epsilon_res C) +| (terminal2 'd'-+> epsilon_res D);;
let pr_bool : 'res ranalist = (terminal2 '0' -+> epsilon_res BFalse) +| (terminal2 '1' -+> epsilon_res BTrue);;
let pr_fact_affect : 'res ranalist = (pr_bool) +| (pr_var ++> fun v -> epsilon_res (Var v));;

(*Opérations booléennes*)
let rec pr_E : 'res ranalist = fun l -> (pr_O +| pr_A +| pr_N +| pr_fact_affect) l
      and 
      pr_O : 'res ranalist = fun l -> (pr_fact_affect ++> fun o1 -> terminal2 '+' -+> pr_E ++> fun o2 -> epsilon_res (Or(o1,o2))) l
      and 
      pr_A : 'res ranalist = fun l -> (pr_fact_affect ++> fun a1 -> terminal2 '.' -+> pr_E ++> fun a2 -> epsilon_res (And(a1,a2))) l
      and 
      pr_N : 'res ranalist = fun l-> (terminal2 '!' --> terminal2 '(' -+> pr_E ++> fun e -> terminal2 ')' -+> epsilon_res (Not(e))) l
;;

let _ = pr_E(list_of_string "a.b");;
(*-------------------*)

let pr_affect : 'res ranalist = pr_var ++> fun v ->  terminal2 ':' --> terminal2 '=' -+> pr_E ++> fun e ->  epsilon_res (Assign(v,e));;
let rec pr_seq : 'res ranalist = fun l -> ((pr_instr ++> fun s1 -> pr_seq ++> fun s2 -> epsilon_res(Seq(s1,s2))) +| (terminal2 ';' -+> pr_seq ++> fun s1 -> epsilon_res s1) +| (epsilon -+> epsilon_res (Skip))) l 
  and
  pr_instr : 'res ranalist = fun l -> (pr_affect +| pr_if +| pr_while) l
    and
    pr_if : 'res ranalist = fun l -> (terminal2 'i' --> terminal2 '(' -+> pr_E ++> fun c -> terminal2 ')' --> terminal2 '{' -+> pr_seq ++> fun e1 -> terminal2 '}' --> terminal2 '{' -+> pr_seq ++> fun e2 -> terminal2 '}' -+> epsilon_res(If(c,e1,e2)))l
    and
    pr_while : 'res ranalist = fun l -> (terminal2 'w' --> terminal2 '(' -+> pr_E ++> fun c ->  terminal2 ')' --> terminal2 '{' -+> pr_seq ++> fun e -> terminal2 '}' -+>  epsilon_res (While(c,e))) l 
  ;;

let _ = pr_seq(list_of_string "a:=1;b:=1;c:=1;w(a){i(c){c:=0;a:=b}{b:=0;c:=a}}");;
let _ = pr_seq(list_of_string "a:=1;b:=1;c:=1");;
let _ = pr_seq(list_of_string "a:=1;b:=0;c:=a.b");;

(*type state : il contient les valeurs des variables, associée à ces dernières. On a fait ce choix car plus clair à la lecture*)

type state = (var * bool) list;; 
let convert x = 
  match x with 
  | Var(A) -> A
  | Var(B) -> B
  | Var(C)-> C
  | Var(D) -> D
  |_ -> failwith "Pas une variable"

let rec get (v:var) (s: state) =
  match s with
  | [] -> failwith "pas la variable"
  | (x,b)::xs -> if x = v then b else get v xs
;;

let rec update (var:var) (v:bool) (s:state) : state =
  match var,s with
  |var,[] -> (var,v)::[]
  |var,(x,b)::xs -> if x=var then (x,v)::xs else (x,b)::update var v xs
;;

let rec eval (b : expr) (s : state) =
  match b with
  | BTrue -> true
  | BFalse -> false
  | Var (x) -> get x s
  | Or(o1,o2) -> if eval o1 s then true else (if eval o2 s then true else false)
  | And(a1,a2) -> if eval a1 s then (if eval a2 s then true else false) else false
  | Not(a1) -> if eval a1 s then false else true
;;

let rec evalW (i : winstr) (s : state) =
  match i with
  | Skip -> s
  | Assign(x,e) -> update x (eval e s) s
  | Seq(e1,e2) -> evalW e2 (evalW e1 s)
  | If(e,i1,i2) -> if (eval e s) then evalW i1 s else evalW i2 s
  | While(e,i1) as i -> match get (convert e) s with
                          | true  -> let s1 = evalW i1 s in evalW i s1
                          | false -> s
;;

(*Tests EvalW OK*)
let test = Seq(Assign(B,BTrue),Assign(A,Not(BFalse)));;
let test2 = Seq(Assign(A,BTrue),While(Var A,Assign(A,BFalse)));;
let _  = evalW test [];;



let analyst s init = let (win,s) = pr_seq(list_of_string s) in evalW win init;;

(*Tests Analyst avec opérations booléennes OK*)
let _ = analyst "a:=1;b:=  \n1;c: =a.!(b)" [];;
let _ = analyst "b:=1;    \n    c:=     a.!(b)" [(A,true)];;

