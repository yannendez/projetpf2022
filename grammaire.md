#Grammaire du projet de programmation fonctionelle

##Grammaire du langage whileB--
(le caractère . est la concaténation)

*bool* : '0' | '1'

*variable* : 'a' | 'b' | 'c' | 'd'

*fact_affectation* : *bool*
  | *variable* . ';'

*affectation* : *variable* . ':' . '=' .*fact_affectation*
  | *fact_affectation* . *variable*

(pas sûr qu'on en ai besoin) *suite_affectation* : *affection*
  | *affectation* . *suite_affectation*

*corps* : *instructions*

 *while* : 'w' . '(' . *variable* . ')' . '{' . *corps* . '}'

 *if* : 'i' . '(' . *bool* . ')' .  '{' . *corps* . }

 *instructions* = *affectation* | *affectation* .  ';' . *instructions*
